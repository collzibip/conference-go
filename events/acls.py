import json
import requests
from .keys import PEXELS_API_KEY

def get_photo(city, state):
    # Use requests to get a picture from Pexels
    headers ={"Authorization": PEXELS_API_KEY}

    query = f'{city} {state}'
    url = f'https://api.pexels.com/v1/search?query={query}'

    response = requests.get(url, headers=headers)
    picture_url = response.json()["photos"][0]["src"]["original"]
    return {
        "picture_url":picture_url
    }

def get_weather():
    # Use requests to get weather
    response = requests.get("https://jservice.xyz/api/random-clue?valid=true")
    clue = json.loads(response.content)

    # Create a dictionary of data to use containing
    #    answer
    #    question
    #    category title
    fun_fact = {
        "answer": clue["answer"],
        "question": clue["question"],
        "category": clue["category"]["title"]
    }

    # Return the dictionary
    return fun_fact
